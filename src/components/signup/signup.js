import * as React from "react";
import CardActions from "@material-ui/core/CardActions/CardActions";
import CardContent from "@material-ui/core/CardContent/CardContent";
import Card from "@material-ui/core/Card/Card";
import TextField from "@material-ui/core/TextField/TextField";
import Grid from "@material-ui/core/Grid/Grid";
import Button from "@material-ui/core/Button/Button";
import SocialButtons from "../social/social_buttons";
import "./signup.css";

export default class SignUp extends React.Component {

    signup = (form) => {

    };

    render() {
        return (
            <div className="inapp-container">
                <h2>Authors Haven</h2>

                <Card className="card">
                    <CardContent>
                        <h4>Log-In.</h4>
                        <form onSubmit={() => this.signup(this)}>

                            <Grid container spacing={24}>
                                <Grid item xs={12} sm={12}>
                                    <TextField fullWidth={true} helperText="Enter your username" label="username"/>
                                </Grid>
                                {/*<Grid item xs={12} sm={12}>*/}
                                    {/*<TextField fullWidth={true} helperText="Enter your Email" label="Email"*/}
                                               {/*type="email"/>*/}
                                {/*</Grid>*/}
                                <Grid item xs={12} sm={12}>
                                    <TextField fullWidth={true} helperText="Enter your Password" label="Password"
                                               type="password"/>
                                </Grid>
                            </Grid>
                        </form>

                    </CardContent>
                    <CardActions className="action-buttons">
                        <Button size="small" type="submit" variant="raised" color="primary">
                            Submit
                        </Button>
                        <Button size="small" variant="raised" color="secondary">
                            Cancel
                        </Button>
                    </CardActions>
                </Card>
                <h3>OR</h3>
                <Card className="card">
                    <CardContent>
                        <SocialButtons/>
                    </CardContent>
                </Card>
            </div>
        )
    }
}
