import * as React from "react";
import GoogleLogin from "react-google-login";
import FacebookLogin from "react-facebook-login";
import "font-awesome/css/font-awesome.css";
import './social_buttons.css';
import {HashRouter as Router, Route} from "react-router-dom";
import Home from "../home/home";

export default class SocialButtons extends React.Component {

    constructor(props) {
        super(props);
        // Initializes OAuth.io with API key

        this.google = {
            "client_id": "708953386061-9nojfbtkutp49avt3afjvc9nrfsjfpfl.apps.googleusercontent.com",
        };
        this.facebook = {
            "app_id": "473638723112366"
        };
    }

    onFailure = (response) => {
        console.log(response)
    };

    onSuccess = (response) => {
        let profile_obj = response.profileObj;
        console.log(response);
        alert(`Welcome ${profile_obj.name}. You signed in with google.`);
    };

    responseFacebook = (response) => {
        console.log(response);
        alert(`Welcome ${response.name} You signed in with facebook.`);
    };

    render() {
        return (
            <div className="social-container">
                <h3>LogIn with :</h3>
                <div className="buttons">

                    <GoogleLogin
                        className="fa fa-google google material-button-raised"
                        buttonText={" Google"}
                        prompt={"consent"}
                        redirectUri={"/home"}
                        autoLoad={false}
                        responseType={"id_token"}
                        clientId={this.google.client_id}
                        onFailure={this.onFailure}
                        onSuccess={this.onSuccess}
                    />

                    <FacebookLogin
                        appId={this.facebook.app_id}
                        autoLoad={false}
                        size={"medium"}
                        fields="name,email,picture"
                        callback={this.responseFacebook}
                        cssClass={"fa fa-facebook facebook material-button-raised"}
                        textButton={"Facebook"}
                    />

                </div>

                <Router>
                    <Route path={"/home"} component={Home} />
                </Router>
            </div>
        )
    }

}