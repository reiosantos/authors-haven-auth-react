import React from 'react';
import ReactDOM from 'react-dom';
import "./css/common.main.css";
import './index.css';
import App from './App';
//import 'typeface-roboto';
import registerServiceWorker from './registerServiceWorker';

ReactDOM.render(<App />, document.getElementById('root'));
registerServiceWorker();
